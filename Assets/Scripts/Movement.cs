﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    // ----- PARAMETERS -----
    [SerializeField] float mainThrust = 100f;
    [SerializeField] float angularThrust = 10f;
    [SerializeField] AudioClip mainEngine;
    // From inspector, make sure to select the ParticleSystem childed to the rocket prefab
    [SerializeField] ParticleSystem leftThrustParticles;
    [SerializeField] ParticleSystem rightThrustParticles;
    [SerializeField] ParticleSystem mainThrustParticles;

    // ----- CACHE -----
    // Responsible for updating physics
    Rigidbody body;
    // AudioListener to hear the audio
    // AudioSource to play the audio
    // AudioFile is the sound that gets played
    AudioSource audioSrc;

    // ----- METHODS -----
    // Start is called before the first frame update
    void Start()
    {
        body = GetComponent<Rigidbody>();
        audioSrc = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        // Input binding
        ProcessThrust();
        ProcessRotation();
    }

    void ProcessThrust()
    {   
        // GetKey tells us if the user is pressing the key
        if (Input.GetKey(KeyCode.Space))
        {
            // Debug.Log("You're thrusting!!");
            body.AddRelativeForce(mainThrust * Time.deltaTime * Vector3.up);
            if (!audioSrc.isPlaying)
            {
                audioSrc.PlayOneShot(mainEngine);
            }
            if (!mainThrustParticles.isPlaying)
            {
                mainThrustParticles.Play();
            }
        }
        else
        {
            audioSrc.Stop();
            mainThrustParticles.Stop();
        }
    }

    void ProcessRotation()
    {
        // We don't want the user to be able to rotate left and right simultaneously. Priority to left rotation.
        if (Input.GetKey(KeyCode.A))
        {
            rightThrustParticles.Stop();
            if (!leftThrustParticles.isPlaying)
            {
                leftThrustParticles.Play();
            }
            ApplyRotation(angularThrust * Time.deltaTime);
        }
        else if (Input.GetKey(KeyCode.D))
        {
            leftThrustParticles.Stop();
            if (!rightThrustParticles.isPlaying)
            {
                rightThrustParticles.Play();
            }
            ApplyRotation(-angularThrust * Time.deltaTime);
        }
        else
        {
            leftThrustParticles.Stop();
            rightThrustParticles.Stop();
        }
    }

    void ApplyRotation(float amountToRotate)
    {
        // To avoid physics system and our controls to have conflict during collisions:
        // freezing physics system's rotation so it doesn't mess up our manual rotation
        body.freezeRotation = true; 
        transform.Rotate(amountToRotate * Vector3.forward);
        body.freezeRotation = false; // physics system takes over again
    }
}
