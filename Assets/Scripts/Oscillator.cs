﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Oscillator : MonoBehaviour
{
    [SerializeField] Vector3 finalPos;
    [SerializeField] float period = 4f;

    float distance;
    Vector3 direction;
    Vector3 startPos;


    // Start is called before the first frame update
    void Start()
    {
        startPos = transform.position;
        distance = (finalPos - startPos).magnitude / 2;
        direction = (finalPos - startPos).normalized;
    }

    // Update is called once per frame
    void Update()
    {
        // Comparing floats with 0 is risky, it's better to use Mathf.Epsilon, which is a very little number
        float angVel = period > Mathf.Epsilon ? 2 * Mathf.PI / period : 0;
        float movFactor = - distance * Mathf.Cos(Time.time * angVel) + distance; // -cos(x) = sin(x-pi/2)
        transform.position = startPos + direction * movFactor;
    }
}
