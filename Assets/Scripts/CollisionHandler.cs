﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class CollisionHandler : MonoBehaviour
{
    [SerializeField] float reloadTimer = 1f;
    [SerializeField] AudioClip successAudio;
    [SerializeField] AudioClip failureAudio;
    // From inspector, make sure to select the ParticleSystem childed to the rocket prefab
    [SerializeField] ParticleSystem successParticles;
    [SerializeField] ParticleSystem failureParticles;

    AudioSource audioSrc;
    bool isTransitioning = false;
    bool isCollisionDisabled = false;

    void Start()
    {
        audioSrc = GetComponent<AudioSource>();
    }

    void Update() 
    {
        RespondToDebugKeys();
    }

    void RespondToDebugKeys()
    {
        if (Input.GetKeyDown(KeyCode.L))
        {
            LoadNextLevel();
        }
        else if (Input.GetKeyDown(KeyCode.C))
        {
            isCollisionDisabled = !isCollisionDisabled;
        }
    }

    void OnCollisionEnter(Collision other) 
    {
        if (isTransitioning || isCollisionDisabled)
        {
            return;
        }

        int currentLvlIdx = SceneManager.GetActiveScene().buildIndex;
        switch (other.gameObject.tag)
        {
            case "Finish":
                // Load next scene if this we didn't just play the final level, else load 1st scene
                StartLoadSequence(true);
                break;
            case "Friendly":
                // Do nothing when colliding with friendly stuff
                break;
            default:
                // Stuff with no tag is considered hostile: if touched player loses and has to replay the level
                StartLoadSequence(false);
                break;
        }
    }

    void StartLoadSequence(bool victory) 
    {
        // 1. Disble player movement
        // 2. Stop thrusting sound
        // 3. Reload level / load next sequence after 1 sec, playing the correct sound effect
        // 4. Change state to transitioning so that OnCollisionEnter does nothing
        GetComponent<Movement>().enabled = false;
        audioSrc.Stop();
        if (victory)
        {
            audioSrc.PlayOneShot(successAudio);
            successParticles.Play();
            Invoke("LoadNextLevel", reloadTimer);
        }
        else
        {
            audioSrc.PlayOneShot(failureAudio);
            failureParticles.Play();
            Invoke("ReloadLevel", reloadTimer);
        }
        isTransitioning = true;
    }

    void ReloadLevel()
    {
        int currentLvlIdx = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(currentLvlIdx);
    }

    void LoadNextLevel()
    {
        int nextLvlIdx = SceneManager.GetActiveScene().buildIndex + 1;
        SceneManager.LoadScene(nextLvlIdx != SceneManager.sceneCountInBuildSettings ? nextLvlIdx : 0);
    }
}
